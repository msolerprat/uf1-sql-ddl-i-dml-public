# Instal·lació de l'eina per fer diagrames dia

Per fer diagrames del Model Entitat Relació farem servir l'eina dia.

## Instal·lació a Debian estable

Executem com a root l'ordre:

```
# apt-get install dia
```

## Diagrama ER

Dels diferents diagrames que ens proporciona dia, escollim el que es diu ER:

![selecció de diagrames ER](imatges/dia_diagrama_ER.png)


