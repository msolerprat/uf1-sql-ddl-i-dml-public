# El model relacional

+ Les **relacions** poden ser concebudes com a representacions tabulars de les
  dades.

+ La **clau primària** d'una relació distingeix unívocament cada tupla d’una
  relació de la resta.

+ Una **clau forana** està constituïda per un atribut, o per un conjunt
  d’atributs, de l’esquema d’una relació, que serveix per relacionar les seves
tuples amb les tuples d’una altra relació de la base de dades (o amb les tuples
d’ella mateixa, en alguns casos).

+ La **integritat referencial** implica que, per a qualsevol tupla, la
  combinació de valors que adopta el conjunt dels atributs que formen la clau
forana de la relació o bé ha de ser present en la clau primària a la qual fa
referència, o bé ha d’estar constituïda exclusivament per valors nuls (si els
atributs implicats admeten aquesta possibilitat, i així s’ha estipulat en
definir-ne les propietats). 

## Traducció del model Entitat-Relació al model relacional

### Entitats

**Les entitats es transformen en relacions**:

+ Els atributs de l’entitat originària seran els atributs de la relació
  resultant.

+ La clau primària de l’entitat originària serà la clau primària de la relació
  resultant.

	Model E-R:

	![clau primària al model E-R](imatges/clau_primaria_model_E-R.png)

	Model Relacional:

	ALUMNE(**DNI**, Nom, Cognoms)

### Interrelacions

Les interrelacions binàries de connectivitat 1-1 o 1-N originen claus foranes
en relacions ja existents.

+ Connectivitat 1:1

	- Al Model E-R:

		![interrelació 1:1 al model E-R](imatges/interrelacio_1:1_model_E-R.png)

	- Al Model Relacional:

		Tenim dues possibilitats de transformació, depenent de si col·loquem la clau forana a l'entitat PROFESSOR o a l'entitat DEPARTAMENT.

		DEPARTAMENT(**Codi**, Descripció)

		PROFESSOR(**DNI**, Nom, Cognoms, CodiDepartamentCoordina) ON {CodiDepartamentCoordina} REFERENCIA DEPARTAMENT i CodiDepartamentCoordina admet valors NULS.

		L'altra possible transformació és:
	
		PROFESSOR(**DNI**, Nom, Cognoms)
			  
		DEPARTAMENT(**Codi**, Descripció, DNIProfessor) ON {DNIProfessor} REFERENCIA PROFESSOR

		Al 1er cas veiem que hi hauria molts casos, professors que no són coordinadors de departament, que tindrien aquell camp com a valor NULL. Aquesta gran quantitat de valors NULL ocupen un espai innecessari. Per tant la 2a transformació és molt més adequada.


+ Connectivitat 1:n

	- Al Model E-R:
		
		Aquest exemple contempla que un professor podria no estar assignat a
cap departament.

		![interrelació 1:n al model E-R](imatges/interrelacio_1:n_model_E-R.png)

	- Al Model Relacional:

		Quant tenim una connectivitat 1:n la clau forana s'haurà d'afegir
obligatòriament a la relació derivada de l'entitat del costat N. Altrament ens
trobaríem amb un atribut a la relació Departament multivalor (tots els profes
que treballen en aquell departament) i això no està permès dintre del model
relacional. Per tant ja només hi ha una possibilitat:
	
		PROFESSOR(**DNI**, Nom, Cognoms, CodiDepartamentTreballa) ON {CodiDepartamentTreballa} REFERENCIA DEPARTAMENT i CodiDepartamentTreballa ADMET VALORS NULS

		DEPARTAMENT(**Codi**, Descripcio)

		Aquí ja no podem evitar els valors NULS però, tal com s'ha dit, l'altra
possibilitat no està admesa pel model relacional.


> **Les interrelacions binàries de connectivitat M-N i totes les n-àries d’ordre superior a 2 sempre es transformen en noves relacions.**

És a dir, a més a més, de les entitats que es transformen en relacions, la
interrelació **també** es tranforma en relació. Veiem-ho amb exemples:

+ Connectivitat m:n

	- Al Model E-R:

		![interrelació m:n al Model E-R](imatges/interrelacio_m:n_model_E-R.png)

	+ Al Model Relacional:

		ALUMNE(**DNI**, Nomm, Cognoms)

		ESPORT(**Codi**, Descripcio)

		i a banda de les relacions derivades de les entitats es crea una nova
relació derivada de la interrelació¹:

		PRACTICA(**DNIAlumne, CodiEsport**, DiaSetmana) ON {DNIAlumne} REFERENCIA ALUMNE i {CodiEsport} REFERENCIA ESPORT

+ Connectivitat m:n:p

	- Al Model E-R:

		![interrelació m:n:p al Model E-R](imatges/interrelacio_m:n:p_model_E-R.png)

	- Al Model Relacional:

		ALUMNE(**DNI**, Nom, Cognoms)

		ESPORT(**Codi**, Descripcio)

		CURS(**Codi**)

		i a banda de les relacions derivades de les entitats es crea una nova
relació derivada de la interrelació:

		PRACTICA(**DNI-Alumne, Codi-Esport, Codi-Curs**, DiaSetmana) ON {DNI-Alumne} REFERENCIA ALUMNE, {Codi-Esport} REFERENCIA ESPORT i {Codi-Curs} REFERENCIA CURS

+ Interrelació **recursiva** de connectivitat **1:n**

	- Al Model E-R:

		![interrelació recursiva de grau de connectivitat 1:n al Model E-R](imatges/interrelacio_recursiva_1:n_model_E-R.png)

	- Al Model Relacional:

		ALUMNE(**DNI**, Nom, Cognoms, DNI-Delegat) ON {DNI-Delegat} referencia ALUMNE

+ Interrelació **recursiva** de connectivitat **m:n**

	- Al Model E-R:

		![interrelació recursiva de connectivitat m:n al Model E-R](imatges/interrelacio_recursiva_m:n_model_E-R.png)

	- Al Model Relacional:

		Aquí ens torna a passar com quan treballàvem amb relacions m:n no recursives, la interrelació crea una nova relació.²

		ASSIGNATURA(**Codi**, Descripcio)

		PRERREQUISIT(**Codi-Assignatura, Codi-Assignatura-Prerrequisit**) ON {Codi-Assignatura} REFERENCIA ASSIGNATURA i {Codi-Assignatura-Prerrequisit} REFERENCIA ASSIGNATURA

### Generalització i especialització

En aquests casos, tant l’entitat superclasse com les entitats de tipus
subclasse es transformen en noves relacions.³

+ Generalitzacio/Especialització al Model E-R
	
	![generalització especialització al Model E-R](imatges/generalitzacio-especialitzacio_model_E-R.png)

+ Generalitzacio/Especialització al Model Relacional:

	PERSONA(**DNI**, Nom, Cognom, Telefon)

	PROFESSOR(**DNI**, Sou) ON {DNI} REFERENCIA PERSONA

	ALUMNE(**DNI**) ON {DNI} REFERENCIA PERSONA

	INFORMATIC(**DNI**, EspecialitatMaquinaria, EspecialitatProgramari) ON {DNI} REFERENCIA PROFESSOR

	ADMINISTRATIU(**DNI**, Titulacio, Especialitat) ON {DNI} REFERENCIA PROFESSOR


## Notes

¹ La clau primària de la nova relació estarà formada pels atributs de les claus
primàries de les entitats que relaciona. D'altra banda, si existeixen atributs
d'interrelació es tranformaran en atributs de la nova relació.

² La clau primària d'aquesta nova relació (estem a l'exemple de recursivitat)
està formada pels atributs que formen la clau primària de l'entitat original,
però dos cops !! De manera que haurem de posar noms diferents perquè no hi hagi
cap ambigüitat.

³ Quan passem d'entitat superclasse a _relació superclasse_ es manté la mateixa
clau primària. A més haurà de tenir els atributs comuns a tota especialització.
D'altra banda, quan passem d'entitat subclasse a _relació subclasse_ també es
manté la mateixa clau primàri, que al seu torn és la mateixa de l'entitat
superclasse. I al mateix temps aquesta clau primària actua com a clau forana en
referenciar la relació de la superclasse.  

## LINKS

+ Extret dels [apunts de l'IOC](https://ioc.xtec.cat/materials/FP/Recursos/fp_asx_m02_/web/fp_asx_m02_htmlindex/WebContent/u3/a1/continguts.html)
